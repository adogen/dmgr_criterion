#ifndef MARKET_DATA_H
#define MARKET_DATA_H

typedef long long VolumeType;

// stock level one
typedef struct StockL1MarketData {
  char trading_day[9];      // 交易日
  char instrument_id[31];   // 合约代码
  char exchange_id[16];     // 交易所代码
  char local_time[16];      // 本地转发时间
  char update_time[16];     // 最后修改时间
  double last_price;        // 最新价
  VolumeType last_volume;   // 最新成交量
  double last_amount;       // 最新成交金额
  double pre_close_price;   // 昨收盘
  double adjusted_pcp;      // 除权调整后的收盘价
  double open_price;        // 今开盘
  double highest_price;     // 最高价
  double lowerst_price;     // 最低价
  VolumeType volume;        // 成交总数量
  double turnover;          // 成交总金额
  int transactions;         // 成交笔数
  double close_price;       // 今收盘
  double bid_price1;        // 申买价一
  VolumeType bid_volume1;   // 申买量一
  double ask_price1;        // 申卖价一
  VolumeType ask_volume1;   // 申卖量一
  double bid_price2;        // 申买价二
  VolumeType bid_volume2;   // 申买量二
  double ask_price2;        // 申卖价二
  VolumeType ask_volume2;   // 申卖量二
  double bid_price3;        // 申买价三
  VolumeType bid_volume3;   // 申买量三
  double ask_price3;        // 申卖价三
  VolumeType ask_volume3;   // 申卖量三
  double bid_price4;        // 申买价四
  VolumeType bid_volume4;   // 申买量四
  double ask_price4;        // 申卖价四
  VolumeType ask_volume4;   // 申卖量四
  double bid_price5;        // 申买价五
  VolumeType bid_volume5;   // 申买量五
  double ask_price5;        // 申卖价五
  VolumeType ask_volume5;   // 申卖量五
  double average_price;     // 当日均价
  VolumeType implied_delta; // 内盘
  VolumeType implied_gamma; // 外盘
} StockL1MarketData;

// stock level two
typedef struct StockL2MarketData {
} StockL2MarketData;

typedef struct CTPMarketData {
  char trading_day[9];         // 交易日
  char instrument_id[31];      // 合约代码
  char exchange_id[9];         // 交易所代码
  char exchange_inst_id[31];   // 合约在交易所的代码
  double last_price;           // 最新价
  double pre_settlement_price; // 上次结算价
  double pre_close_price;      // 昨收盘
  double pre_open_interest;    // 昨持仓量
  double open_price;           // 今开盘
  double highest_price;        // 最高价
  double lowerst_price;        // 最低价
  int volume;                  // 数量
  double turnover;             // 成交金额
  double open_interest;        // 持仓量
  double close_price;          // 今收盘
  double settlement_price;     // 本次结算价
  double upper_limit_price;    // 涨停板价
  double lower_limit_price;    // 跌停板价
  double pre_delta;            // 昨虚实度
  double curr_delta;           // 今虚实度
  char update_time[9];         // 最后修改时间
  int update_millisec;         // 最后修改毫秒
  double bid_price1;           // 申买价一
  int bid_volume1;             // 申买量一
  double ask_price1;           // 申卖价一
  int ask_volume1;             // 申卖量一
  double bid_price2;           // 申买价二
  int bid_volume2;             // 申买量二
  double ask_price2;           // 申卖价二
  int ask_volume2;             // 申卖量二
  double bid_price3;           // 申买价三
  int bid_volume3;             // 申买量三
  double ask_price3;           // 申卖价三
  int ask_volume3;             // 申卖量三
  double bid_price4;           // 申买价四
  int bid_volume4;             // 申买量四
  double ask_price4;           // 申卖价四
  int ask_volume4;             // 申卖量四
  double bid_price5;           // 申买价五
  int bid_volume5;             // 申买量五
  double ask_price5;           // 申卖价五
  int ask_volume5;             // 申卖量五
  double average_price;        // 当日均价
  char action_day[9];          // 业务日期
} CTPMarketData;

typedef enum MarketDataType {
  CTP,
  STOCK_L1,
  STOCK_L2
} MarketDataType;

typedef union MarketDataBody {
  CTPMarketData ctp;
  StockL1MarketData stock_l1;
  StockL2MarketData stock_l2;
} MarketDataBody;

typedef struct MarketData {
  char instrument_id[32];
  char update_time[16];
  long received_sec;
  long received_usec;
  MarketDataType type;
  MarketDataBody body;
} MarketData;

#endif /* MARKET_DATA_H */