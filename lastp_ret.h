#include "../dmgr_base.h"

#ifndef DMGR_FUNC_LASTP_RET_H
#define DMGR_FUNC_LASTP_RET_H

class DmgrLastpRet : public DmgrBase {
public:
  int Init(MarketDataType market_data_type,
           std::map<std::string, std::list<std::string> > dmgr_group,
           std::map<std::string, double> adj_pcp, double default_value) {
    if (DmgrBase::Init(market_data_type, dmgr_group, adj_pcp, default_value) <
        0)
      return -1;

    return 0;
  }

  double CalcRet(std::string instrument_id, double last_price) {
    double ret;
    auto adj_pcp_it = adj_pcp_.find(instrument_id);
    if (adj_pcp_it != adj_pcp_.end() && adj_pcp_it->second > 0) {
      ret = last_price / adj_pcp_it->second - 1;
    } else {
      ret = 0;
    }

    latest_value_[instrument_id] = ret;
    return ret;
  }

  double InsertMarketDataCTP(const MarketData *market_data) {
    return CalcRet(market_data->instrument_id,
                   market_data->body.ctp.last_price);
  }
  double InsertMarketDataStockL1(const MarketData *market_data) {
    return CalcRet(market_data->instrument_id,
                   market_data->body.stock_l1.last_price);
  }
  double InsertMarketDataStockL2(const MarketData *market_data) {
    if (market_data->instrument_id) {
      ;
    }
    return default_value_;
  }
};

#endif /* DMGR_FUNC_LASTP_RET_H */
