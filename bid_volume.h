#include "../dmgr_base.h"

#ifndef DMGR_FUNC_BID_VOLUME_H
#define DMGR_FUNC_BID_VOLUME_H

class DmgrBidVolume : public DmgrBase {
public:
  class BidBook {
  public:
    BidBook() { total_volume_ = 0; }

    int Erase(long price) {
      if (price <= 0) {
        order_book_.clear();
        total_volume_ = 0;
      } else {
        auto iter = order_book_.begin();
        while (iter != order_book_.end()) {
          if (iter->first >= price) {
            total_volume_ -= iter->second;
            order_book_.erase(iter);
            iter = order_book_.begin();
          } else {
            break;
          }
        }
      }

      return 0;
    }

    int Insert(long price, long volume) {
      auto iter = order_book_.find(price);
      if (iter != order_book_.end()) {
        total_volume_ -= iter->second;
        total_volume_ += volume;
        iter->second = volume;
      } else {
        total_volume_ += volume;
        order_book_[price] = volume;
      }
      return 0;
    }

    long get_total_volume() { return total_volume_; }

  private:
    std::map<long, long, std::greater<long> > order_book_;
    long total_volume_;
  };

public:
  int Init(MarketDataType market_data_type,
           std::map<std::string, std::list<std::string> > dmgr_group,
           std::map<std::string, double> adj_pcp, double default_value) {
    if (DmgrBase::Init(market_data_type, dmgr_group, adj_pcp, default_value) <
        0)
      return -1;

    try {
      for (auto iter : latest_value_) {
        bidv_[iter.first];
      }
    }
    catch (const std::exception &e) {
      log_error("%s", e.what());
      return -1;
    }

    return 0;
  }

  double InsertMarketDataCTP(const MarketData *market_data) {
    auto iter = bidv_.find(market_data->instrument_id);
    if (iter != bidv_.end() && market_data->body.ctp.bid_price1 <= 0.0001 &&
        market_data->body.ctp.ask_price1 <= 0.0001)
      return default_value_;

    long price;
    long volume;

    price = long(market_data->body.ctp.bid_price1 * 1000 + 0.5);
    iter->second.Erase(price);

    price = long(market_data->body.ctp.bid_price1 * 1000 + 0.5);
    volume = market_data->body.ctp.bid_volume1;
    iter->second.Insert(price, volume);

    return iter->second.get_total_volume();
  }

  double InsertMarketDataStockL1(const MarketData *market_data) {
    auto iter = bidv_.find(market_data->instrument_id);

    if (iter != bidv_.end() &&
        market_data->body.stock_l1.bid_price1 <= 0.0001 &&
        market_data->body.stock_l1.ask_price1 <= 0.0001)
      return default_value_;

    long price;
    long volume;

    price = long(market_data->body.stock_l1.bid_price5 * 100 + 0.5);
    iter->second.Erase(price);

    price = long(market_data->body.stock_l1.bid_price5 * 100 + 0.5);
    volume = market_data->body.stock_l1.bid_volume5;
    iter->second.Insert(price, volume);

    price = long(market_data->body.stock_l1.bid_price4 * 100 + 0.5);
    volume = market_data->body.stock_l1.bid_volume4;
    iter->second.Insert(price, volume);

    price = long(market_data->body.stock_l1.bid_price3 * 100 + 0.5);
    volume = market_data->body.stock_l1.bid_volume3;
    iter->second.Insert(price, volume);

    price = long(market_data->body.stock_l1.bid_price2 * 100 + 0.5);
    volume = market_data->body.stock_l1.bid_volume2;
    iter->second.Insert(price, volume);

    price = long(market_data->body.stock_l1.bid_price1 * 100 + 0.5);
    volume = market_data->body.stock_l1.bid_volume1;
    iter->second.Insert(price, volume);

    latest_value_[market_data->instrument_id] = iter->second.get_total_volume();

    return iter->second.get_total_volume();
  }

  double InsertMarketDataStockL2(const MarketData *market_data) {
    if (market_data)
      ;
    return default_value_;
  }

private:
  std::map<std::string, BidBook> bidv_;
};

#endif /* DMGR_FUNC_BID_VOLUME_H */
