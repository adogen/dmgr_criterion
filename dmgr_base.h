#include <map>
#include <list>

class Dmgr {
public:
  virtual ~Dmgr() {}

  virtual double InsertMarketData(const MarketData *market_data) = 0;
  virtual double GetLatestValue(const char *instrument) = 0;
};


class DmgrBase : public Dmgr {
public:
  virtual ~DmgrBase() {}

  int Init(MarketDataType market_data_type,
           const std::map<std::string, std::list<std::string> > &dmgr_group,
           const std::map<std::string, double> &adj_pcp, double default_value) {
    try {
      market_data_type_ = market_data_type;
      dmgr_group_ = dmgr_group;
      adj_pcp_ = adj_pcp;
      default_value_ = default_value;

      for (const auto &it : dmgr_group_) {
        latest_value_[it.first] = default_value_;
        for (const auto &inst : it.second) {
          latest_value_[inst] = default_value_;
        }
      }
    }
    catch (const std::exception &e) {
      //log_error("%s", e.what());
      return -1;
    }
    return 0;
  }

  double GetLatestValue(const char *instrument) {
    auto it = latest_value_.find(instrument);
    if (it != latest_value_.end()) {
      return it->second;
    } else {
      return default_value_;
    }
  }

  double InsertMarketData(const MarketData *market_data) {
    if (market_data->type != market_data_type_) {
      return default_value_;
    }

    if (latest_value_.find(market_data->instrument_id) == latest_value_.end()) {
      return default_value_;
    }

    switch (market_data_type_) {
    case CTP: {
      return InsertMarketDataCTP(market_data);
      break;
    }
    case STOCK_L1: {
      return InsertMarketDataStockL1(market_data);
      break;
    }
    case STOCK_L2: {
      return InsertMarketDataStockL2(market_data);
      break;
    }
    default:
      return default_value_;
      break;
    }
  }

  virtual double InsertMarketDataCTP(const MarketData *) = 0;
  virtual double InsertMarketDataStockL1(const MarketData *) = 0;
  virtual double InsertMarketDataStockL2(const MarketData *) = 0;

protected:
  MarketDataType market_data_type_;
  std::map<std::string, std::list<std::string> > dmgr_group_;
  std::map<std::string, double> adj_pcp_;
  double default_value_;

  std::map<std::string, double> latest_value_;
};
