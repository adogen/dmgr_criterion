#include "../dmgr_base.h"

#ifndef DMGR_FUNC_GROUP_RET_H
#define DMGR_FUNC_GROUP_RET_H

class DmgrGroupRet : public DmgrBase {
public:
  int Init(MarketDataType market_data_type,
           std::map<std::string, std::list<std::string> > dmgr_group,
           std::map<std::string, double> adj_pcp, double default_value) {
    if (DmgrBase::Init(market_data_type, dmgr_group, adj_pcp, default_value) <
        0)
      return -1;

    try {
      for (auto iter : latest_value_) {
        ret_[iter.first] = 0;
      }
    }
    catch (const std::exception &e) {
      log_error("%s", e.what());
      return -1;
    }

    return 0;
  }

  double CalcRet(std::string instrument_id, double last_price) {
    auto adj_pcp_it = adj_pcp_.find(instrument_id);
    if (adj_pcp_it != adj_pcp_.end() && adj_pcp_it->second > 0) {
      ret_[instrument_id] = last_price / adj_pcp_it->second - 1;
    }

    double ret = 0;
    auto it = dmgr_group_.find(instrument_id);
    if (it == dmgr_group_.end() || it->second.size() == 0) {
      ret = 0;
    } else {
      for (const auto &instrument : it->second) {
        ret += ret_[instrument];
      }
      ret /= it->second.size();
    }

    latest_value_[instrument_id] = ret;
    return ret;
  }

  double InsertMarketDataCTP(const MarketData *market_data) {
    return CalcRet(market_data->instrument_id,
                   market_data->body.ctp.last_price);
  }

  double InsertMarketDataStockL1(const MarketData *market_data) {
    return CalcRet(market_data->instrument_id,
                   market_data->body.stock_l1.last_price);
  }

  double InsertMarketDataStockL2(const MarketData *market_data) {
    if (market_data->instrument_id) {
      ;
    }
    return default_value_;
  }

private:
  std::map<std::string, double> ret_;
};

#endif /* DMGR_FUNC_GROUP_RET_H */
