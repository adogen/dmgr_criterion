#include "../dmgr_base.h"

#ifndef DMGR_FUNC_ORDER_FLOW_BID_H
#define DMGR_FUNC_ORDER_FLOW_BID_H

class DmgrOrderFlowBid : public DmgrBase {
public:
  int Init(MarketDataType market_data_type,
           std::map<std::string, std::list<std::string> > dmgr_group,
           std::map<std::string, double> adj_pcp, double default_value) {
    if (DmgrBase::Init(market_data_type, dmgr_group, adj_pcp, default_value) <
        0)
      return -1;

    return 0;
  }

  long max(long a, long b) { return a > b ? a : b; }

  long doubleToLong(double a) {
      long b = long(a * 100 + 0.5);
      return b;
  }

  long CalcBidFlowL1(const char *instrument_id, const MarketData *market_data, std::map<long, long> pv_pair ) {
    long orderflow = 0;
    long bid_price[5] = { doubleToLong(market_data->body.stock_l1.bid_price1), doubleToLong(market_data->body.stock_l1.bid_price2),
                          doubleToLong(market_data->body.stock_l1.bid_price3), doubleToLong(market_data->body.stock_l1.bid_price4),
                          doubleToLong(market_data->body.stock_l1.bid_price5) };
    long bid_volume[5] = { market_data->body.stock_l1.bid_volume1, market_data->body.stock_l1.bid_volume2,
                           market_data->body.stock_l1.bid_volume3, market_data->body.stock_l1.bid_volume4,
                           market_data->body.stock_l1.bid_volume5 };
    auto pos = pv_pair.find(0);
    if ( pos != pv_pair.end() ) {
        pv_pair.erase(pos);
    }
    for ( int i = 0; i < 5; i++ ) {
        long price_l = bid_price[i];
        if ( pv_pair.empty() || price_l <= 0 ) continue;
        if ( price_l > pv_pair.end()->first ) {
            orderflow += 2 * bid_volume[i];
        }
        else if ( price_l >= pv_pair.begin()->first ) {
            auto it = pv_pair.find(price_l);
            if ( it != pv_pair.end() ) {
                orderflow += bid_volume[i] - it->second;
                pv_pair.erase(it);
            }
            else orderflow += bid_volume[i];
        }
    }
    if (!pv_pair.empty()) {
        auto it = pv_pair.begin();
        for (; it != pv_pair.end(); ++it) {
            if (it->second <= bid_price[0] && it->second >= bid_price[4]) {
                orderflow += (-1) * it->second;
            }
        }
    }
    latest_value_[instrument_id] = orderflow;
    return orderflow;
  }

  double InsertMarketDataCTP(const MarketData *market_data) {
    if (market_data->instrument_id) {
      ;
    }
    return default_value_;
  }
  double InsertMarketDataStockL1(const MarketData *market_data) {
    if (prev_data_.find(market_data->instrument_id) != prev_data_.end()) {
        MarketData pdata = prev_data_[market_data->instrument_id];
        std::map<long, long> pv_pair;
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.bid_price5), 0), pdata->body.stock_l1.bid_volume5));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.bid_price4), 0), pdata->body.stock_l1.bid_volume4));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.bid_price3), 0), pdata->body.stock_l1.bid_volume3));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.bid_price2), 0), pdata->body.stock_l1.bid_volume2));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.bid_price1), 0), pdata->body.stock_l1.bid_volume1));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.ask_price1), 0), (-1) * pdata->body.stock_l1.ask_volume1));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.ask_price2), 0), (-1) * pdata->body.stock_l1.ask_volume2));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.ask_price3), 0), (-1) * pdata->body.stock_l1.ask_volume3));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.ask_price4), 0), (-1) * pdata->body.stock_l1.ask_volume4));
        pv_pair.insert(std::pair<long, long>(max(doubleToLong(pdata->body.stock_l1.ask_price5), 0), (-1) * pdata->body.stock_l1.ask_volume5));
        prev_data_[market_data->instrument_id] = market_data;
        return CalcBidFlowL1(market_data->instrument_id, market_data, pv_pair);
    }
    else {
        prev_data_.insert(std::pair<std::string, MarketData*>(market_data->instrument_id, market_data));
        return 0;
    }
  }
  double InsertMarketDataStockL2(const MarketData *market_data) {
    if (market_data->instrument_id) {
      ;
    }
    return default_value_;
  }

private:
    std::map<std::string, MarketData*> prev_data_;
};

#endif /* DMGR_FUNC_MIDP_RET_H */
