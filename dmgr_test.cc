#include <string.h>

#include <criterion/criterion.h>
#include "../dmgr/dmgr.h"


static void insert_data(Dmgr *order_flow_bid, Dmgr *order_flow_ask, MarketData *data,
                        const char *instrument_id, double bidp[], long bidv[],
                        double askp[], long askv[],
                        double order_flow_ask_expected_value,
                        double order_flow_bid_expected_value) {
  strcpy(data->instrument_id, instrument_id);
  data->body.stock_l1.bid_price1 = bidp[1];
  data->body.stock_l1.bid_price2 = bidp[2];
  data->body.stock_l1.bid_price3 = bidp[3];
  data->body.stock_l1.bid_price4 = bidp[4];
  data->body.stock_l1.bid_price5 = bidp[5];
  data->body.stock_l1.bid_volume1 = bidv[1];
  data->body.stock_l1.bid_volume2 = bidv[2];
  data->body.stock_l1.bid_volume3 = bidv[3];
  data->body.stock_l1.bid_volume4 = bidv[4];
  data->body.stock_l1.bid_volume5 = bidv[5];

  data->body.stock_l1.ask_price1 = askp[1];
  data->body.stock_l1.ask_price2 = askp[2];
  data->body.stock_l1.ask_price3 = askp[3];
  data->body.stock_l1.ask_price4 = askp[4];
  data->body.stock_l1.ask_price5 = askp[5];
  data->body.stock_l1.ask_volume1 = askv[1];
  data->body.stock_l1.ask_volume2 = askv[2];
  data->body.stock_l1.ask_volume3 = askv[3];
  data->body.stock_l1.ask_volume4 = askv[4];
  data->body.stock_l1.ask_volume5 = askv[5];

  double value;
  value = order_flow_bid->InsertMarketData(data);
  cr_assert_float_eq(value, order_flow_bid_expected_value, 1e-10,
                     "bid_volume insert %s %.2lf %.2lf   %lf != %lf",
                     instrument_id, bidp[0], askp[0], value,
                     order_flow_bid_expected_value);

  value = order_flow_ask->InsertMarketData(data);
  cr_assert_float_eq(value, order_flow_ask_expected_value, 1e-10,
                     "ask_volume insert %s %.2lf %.2lf   %lf != %lf",
                     instrument_id, bidp[0], askp[0], value,
                     order_flow_ask_expected_value);
}

Test(dmgr_stock_l1, bid_volume_and_ask_volume) {
  const char *dmgr_group_json = {
    "{"
    "\"000001\": [],"
    "\"000002\": [\"000001\"],"
    "\"000003\": [\"000001\", \"000002\"],"
    "\"000004\": [\"000001\", \"000002\", \"000003\"],"
    "\"000005\": [\"000001\", \"000002\", \"000003\", \"000004\", \"000005\"]"
    "}"
  };

  const char *adj_pcp_json = { "{"
                               "\"000001\": 10.0,"
                               "\"000002\": 20.0,"
                               "\"000003\": 30.0,"
                               "\"000004\": 40.0,"
                               "\"000005\": 50.0"
                               "}" };

  Dmgr *order_flow_bid =
      Dmgr_New("order_flow_bid", STOCK_L1, dmgr_group_json, adj_pcp_json);
  cr_assert(order_flow_bid);
  Dmgr *order_flow_ask =
      Dmgr_New("order_flow_ask", STOCK_L1, dmgr_group_json, adj_pcp_json);
  cr_assert(order_flow_ask);

  MarketData data;
  memset(&data, 0, sizeof(data));
  data.type = STOCK_L1;

  double bidp[6] = { 0 };
  long bidv[6] = { 0 };
  double askp[6] = { 0 };
  long askv[6] = { 0 };

  askp[1] = 9.1, askp[2] = 9.2, askp[3] = 9.3, askp[4] = 9.4, askp[5] = 9.5;
  askv[1] = 1, askv[2] = 2, askv[3] = 3, askv[4] = 4, askv[5] = 5;
  bidp[5] = 8.6, bidp[4] = 8.7, bidp[3] = 8.8, bidp[2] = 8.9, bidp[1] = 9.0;
  bidv[5] = 5, bidv[4] = 4, bidv[3] = 3, bidv[2] = 2, bidv[1] = 1;
  insert_data(bid_volume, ask_volume, &data, "000001", bidp, bidv, askp, askv,
              0, 0);

  askp[1] = 9.0, askp[2] = 9.1, askp[3] = 9.2, askp[4] = 9.3, askp[5] = 9.4;
  askv[1] = 1, askv[2] = 1, askv[3] = 2, askv[4] = 3, askv[5] = 4;
  bidp[5] = 8.5, bidp[4] = 8.6, bidp[3] = 8.7, bidp[2] = 8.8, bidp[1] = 8.9;
  bidv[5] = 5, bidv[4] = 5, bidv[3] = 4, bidv[2] = 3, bidv[1] = 3;
  insert_data(bid_volume, ask_volume, &data, "000001", bidp, bidv, askp, askv,
              2, 1);

  askp[1] = 9.1, askp[2] = 9.2, askp[3] = 9.3, askp[4] = 9.4, askp[5] = 9.5;
  askv[1] = 1, askv[2] = 2, askv[3] = 3, askv[4] = 4, askv[5] = 5;
  bidp[5] = 8.6, bidp[4] = 8.7, bidp[3] = 8.8, bidp[2] = 8.9, bidp[1] = 9.0;
  bidv[5] = 5, bidv[4] = 4, bidv[3] = 3, bidv[2] = 2, bidv[1] = 1;
  insert_data(bid_volume, ask_volume, &data, "000001", bidp, bidv, askp, askv,
              0, 1);

  askp[1] = 9.5, askp[2] = 9.6, askp[3] = 9.7, askp[4] = 9.8, askp[5] = 9.9;
  askv[1] = 5, askv[2] = 6, askv[3] = 7, askv[4] = 8, askv[5] = 9;
  bidp[5] = 9.0, bidp[4] = 9.1, bidp[3] = 9.2, bidp[2] = 9.3, bidp[1] = 9.4;
  bidv[5] = 1, bidv[4] = 1, bidv[3] = 2, bidv[2] = 3, bidv[1] = 4;
  insert_data(bid_volume, ask_volume, &data, "000001", bidp, bidv, askp, askv,
              0, 20);

  askp[1] = 9.5, askp[2] = 9.6, askp[3] = 9.65, askp[4] = 9.8, askp[5] = 9.9;
  askv[1] = 5, askv[2] = 6, askv[3] = 7, askv[4] = 8, askv[5] = 9;
  bidp[5] = 9.0, bidp[4] = 9.1, bidp[3] = 9.2, bidp[2] = 9.3, bidp[1] = 9.4;
  bidv[5] = 1, bidv[4] = 1, bidv[3] = 2, bidv[2] = 3, bidv[1] = 4;
  insert_data(bid_volume, ask_volume, &data, "000001", bidp, bidv, askp, askv,
              0, 0);

}