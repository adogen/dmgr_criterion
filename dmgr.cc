#include "func/lastp_ret.h"
#include "func/midp_ret.h"
#include "func/spread.h"
#include "func/group_ret.h"
#include "func/bid_volume.h"
#include "func/ask_volume.h"


#include "json.hpp"
using json = nlohmann::json;

int parse_adj_pcp(std::map<std::string, double> *adj_pcp,
                  const char *json_str) {
  try {
    json j = json::parse(json_str);
    adj_pcp->clear();
    for (json::iterator it = j.begin(); it != j.end(); ++it) {
      (*adj_pcp)[it.key()] = it.value();
    }
  }
  catch (const std::exception &e) {
    log_error("");
    std::cout << e.what() << "\n";
    adj_pcp->clear();
    return -1;
  }

  return 0;
}

int parse_dmgr_group(std::map<std::string, std::list<std::string> > *dmgr_group,
                     const char *json_str) {
  try {
    json config = json::parse(json_str);
    dmgr_group->clear();
    for (json::iterator it = config.begin(); it != config.end(); ++it) {
      std::string inst = it.key();
      std::list<std::string> list;
      for (auto obj : it.value()) {
        list.push_back(obj.get<std::string>());
      }
      (*dmgr_group)[inst] = list;
    }
  }
  catch (const std::exception &e) {
    log_error("");
    std::cout << e.what() << "\n";
    dmgr_group->clear();
    return -1;
  }

  return 0;
}

#define DMGR_NEW(name, default_value)                                          \
  do {                                                                         \
    Dmgr##name *tmp = new (std::nothrow) Dmgr##name();                         \
    if (tmp != NULL) {                                                         \
      if (tmp->Init(market_data_type, dmgr_group, adj_pcp, default_value) <    \
          0) {                                                                 \
        delete tmp;                                                            \
      } else {                                                                 \
        dmgr = tmp;                                                            \
      }                                                                        \
    }                                                                          \
  } while (0)

Dmgr *Dmgr_New(const char *dmgr_name, MarketDataType market_data_type,
               const std::map<std::string, std::list<std::string> > &dmgr_group,
               const std::map<std::string, double> &adj_pcp) {
  for (const auto &it : dmgr_group) {
    if (adj_pcp.find(it.first) == adj_pcp.end()) {
      log_error("%s not in adj_pcp", it.first.c_str());
      return NULL;
    }
    for (const auto &instrument : it.second) {
      if (adj_pcp.find(instrument) == adj_pcp.end()) {
        log_error("%s not in adj_pcp", instrument.c_str());
        return NULL;
      }
    }
  }

  Dmgr *dmgr = NULL;
  if (strcmp(dmgr_name, "lastp_ret") == 0) {
    DMGR_NEW(LastpRet, 0);

  } else if (strcmp(dmgr_name, "midp_ret") == 0) {
    DMGR_NEW(MidpRet, 0);

  } else if (strcmp(dmgr_name, "spread") == 0) {
    DMGR_NEW(Spread, 0);

  } else if (strcmp(dmgr_name, "group_ret") == 0) {
    DMGR_NEW(GroupRet, 0);

  } else if (strcmp(dmgr_name, "bid_volume") == 0) {
    DMGR_NEW(BidVolume, 0);

  } else if (strcmp(dmgr_name, "ask_volume") == 0) {
    DMGR_NEW(AskVolume, 0);

  } else {
    log_error("invalid dmgr_name %s", dmgr_name);
  }

  return dmgr;
}

Dmgr *Dmgr_New(const char *dmgr_name, MarketDataType market_data_type,
               const char *dmgr_group_json, const char *adj_pcp_json) {
  std::map<std::string, double> adj_pcp;
  std::map<std::string, std::list<std::string> > dmgr_group;
  if (parse_adj_pcp(&adj_pcp, adj_pcp_json) < 0) {
    log_error("");
    return NULL;
  }
  if (parse_dmgr_group(&dmgr_group, dmgr_group_json) < 0) {
    log_error("");
    return NULL;
  }

  return Dmgr_New(dmgr_name, market_data_type, dmgr_group, adj_pcp);
}