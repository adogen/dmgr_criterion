#include "../dmgr_base.h"

#ifndef DMGR_FUNC_SPREAD_H
#define DMGR_FUNC_SPREAD_H

class DmgrSpread : public DmgrBase {
public:
  int Init(MarketDataType market_data_type,
           std::map<std::string, std::list<std::string> > dmgr_group,
           std::map<std::string, double> adj_pcp, double default_value) {
    if (DmgrBase::Init(market_data_type, dmgr_group, adj_pcp, default_value) <
        0)
      return -1;

    return 0;
  }

  double CalcSpread(const char *instrument_id, double bid_price,
                    double ask_price) {
    double spread;
    auto adj_pcp_it = adj_pcp_.find(instrument_id);
    if (adj_pcp_it != adj_pcp_.end() && adj_pcp_it->second > 0 &&
        bid_price > 0.0001 && ask_price > 0.0001) {
      spread = (ask_price - bid_price) / adj_pcp_it->second;
    } else {
      spread = 0;
    }

    latest_value_[instrument_id] = spread;
    return spread;
  }

  double InsertMarketDataCTP(const MarketData *market_data) {
    return CalcSpread(market_data->instrument_id,
                      market_data->body.ctp.bid_price1,
                      market_data->body.ctp.ask_price1);
  }
  double InsertMarketDataStockL1(const MarketData *market_data) {
    return CalcSpread(market_data->instrument_id,
                      market_data->body.stock_l1.bid_price1,
                      market_data->body.stock_l1.ask_price1);
  }
  double InsertMarketDataStockL2(const MarketData *market_data) {
    if (market_data->instrument_id) {
      ;
    }
    return default_value_;
  }
};

#endif /* DMGR_FUNC_SPREAD_H */
